//#include "server.h"
/******* Template (server.cpp) **********/
#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <pthread.h>
//#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
//#include <sys/types.h>
#include <sys/socket.h>
#include <cstring>
#include <vector>
#include <ctime>

struct client
{
    int id;
    int sock;
};

struct client_message
{
    uint32_t nickname_size;
    char* nickname;
    uint32_t  body_size;
    char* body;
};

pthread_mutex_t clnt_m;
std::vector<client> client_sockfds;
std::vector<pthread_t> threads;

void * client_processing(void * arg);
int send_message(int sock, client_message msg, uint32_t time_size, char* msg_time);
struct client_message receive_message(int sock);

int main(int argc, char *argv[]) {

    if (argc < 2)
    {
        std::cout << "Usage: ./server <port>" << std::endl;
        exit(0);
    }

    int sockfd;
    uint16_t portno;
    unsigned int clilen;
//    char buffer[256];
    struct sockaddr_in serv_addr{}, cli_addr{};
//    ssize_t n;
    portno = (uint16_t) atoi(argv[1]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("ERROR opening socket");
        exit(1);
    }

//    bzero((char *) &serv_addr, sizeof(serv_addr));
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);

    clilen = sizeof(cli_addr);

    if (pthread_mutex_init(&clnt_m, nullptr) != 0)
    {
        std::cerr << "mutex failed" << std::endl;
        return 1;
    }

    /* Now bind the host address using bind() call.*/
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
    {
        std::cerr << ("error on binding") << std::endl;
        exit(1);
    }
    int client_id = 0;

    /* Now start listening for the clients, here process will
    go in sleep mode and will wait for the incoming connection */
    if((listen(sockfd, 8))==-1)
    {
        std::cerr<<("listen error: ") << std::endl;
        exit(-1);
    }
    int client_sock;
    while(true)
    {

        if ((client_sock = accept(sockfd,(struct sockaddr *) &cli_addr, &clilen))==-1)
        {
            std::cerr<<("accept error") << std::endl;
            exit(-1);
        }
        client new_client = {client_id, client_sock};
        client_id++;

        pthread_mutex_lock(&clnt_m);
        client_sockfds.push_back(new_client);
        pthread_mutex_unlock(&clnt_m);

        pthread_t thread_id;
        pthread_create(&thread_id, nullptr, &client_processing, &new_client);
        threads.push_back(thread_id);
    }
}


void * client_processing(void * arg)
{
    client self_info = *(client *) arg;
    std::time_t current_time;
    struct tm* current_localtime;
    char msg_time[6];
    uint32_t time_size;

    while(true)
    {
        client_message received_message = receive_message(self_info.sock);
        if ((received_message.body_size <= 0) or (strcmp(received_message.body,"#exit")==0))
        {
            pthread_mutex_lock(&clnt_m);
            for(u_long i=0; i<client_sockfds.size(); i++)
            {
                if (client_sockfds[i].id == self_info.id)
                {
                    close(client_sockfds[i].sock);
                    client_sockfds.erase(client_sockfds.begin() + i);
                    break;
                }
            }
            pthread_mutex_unlock(&clnt_m);
            return nullptr;
        }

        current_time = std::time(nullptr);
        current_localtime = std::localtime(&current_time);

        strftime(msg_time, 5, "%H:%M", current_localtime);
        time_size = (uint32_t)strlen(msg_time);

        for(auto client_sock : client_sockfds)
            send_message(client_sock.sock, received_message, time_size, msg_time);
    }
}


struct client_message receive_message(int sock)
{
    client_message recv_msg{};

    // read size of nickname
    ssize_t name_size_len = recv(sock, &recv_msg.nickname_size, sizeof(recv_msg.nickname_size), 0 );
    if (name_size_len == 0)
        return {0, nullptr, 0, nullptr};
//        return {.nickname_size = 0, .nickname = nullptr, .body_size = 0, .body = nullptr};

    recv_msg.nickname_size = ntohl(recv_msg.nickname_size);

    // allocate memory for nickname and read it
    recv_msg.nickname = (char*)malloc(recv_msg.nickname_size);
    ssize_t nickname_len = recv(sock, recv_msg.nickname, recv_msg.nickname_size, 0);
    if (nickname_len < 1)
        return {0, nullptr, 0, nullptr};

    // read size of message body
    ssize_t body_size_len = recv(sock, &recv_msg.body_size, sizeof(recv_msg.body_size), 0 );
    if (body_size_len == 0)
        return {0, nullptr, 0, nullptr};

    recv_msg.body_size = ntohl(recv_msg.body_size);

    // allocate memory for message and read it
    recv_msg.body = (char*)malloc(recv_msg.body_size);
    ssize_t body_len = recv(sock, recv_msg.body, recv_msg.body_size, 0 );
    if (body_len == 0)
        return {0, nullptr, 0, nullptr};
    return recv_msg;
}

int send_message(int sock, client_message msg, uint32_t time_size, char* msg_time)
{
    ssize_t res;
    msg.nickname_size = htonl(msg.nickname_size);
    msg.body_size = htonl(msg.body_size);
    res = send(sock, &msg.nickname_size, sizeof(msg.nickname_size), 0);
    if (res != sizeof(uint32_t))
        return -1;

    res = send(sock, msg.nickname, ntohl(msg.nickname_size), 0);
    if (res != ntohl(msg.nickname_size))
        return -1;

    res = send(sock, &msg.body_size, sizeof(msg.body_size), 0);
    if (res != sizeof(uint32_t))
        return -1;

    res = send(sock, msg.body, ntohl(msg.body_size), 0);
    if (res != ntohl(msg.body_size))
        return -1;

    time_size = htonl(time_size);
    res = send(sock, &time_size, sizeof(time_size), 0);
    if (res != sizeof(uint32_t))
        return -1;

    res = send(sock, msg_time, ntohl(time_size), 0);
    if (res != ntohl(time_size))
        return -1;

    return 0;
}
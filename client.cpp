#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <cstring>
#include <pthread.h>

pthread_mutex_t m;
pthread_mutex_t flag_m;

struct server_message
{
    uint32_t nickname_size;
    char* nickname;
    uint32_t  body_size;
    char* body;
    uint32_t  date_size;
    char* date;
};

struct client_message
{
    uint32_t nickname_size;
    char* nickname;
    uint32_t  body_size;
    char* body;
};

struct self_info{
    uint32_t nickname_size;
    char * nickname;
    int * sock;
    bool socket_is_active;
};

void * receive_message(void *);
void * send_message(void *);
ssize_t receive_data(const int *, server_message *);


int main(int argc, char *argv[]) {

    if (argc < 4) {
        std::cerr << "Usage: ./client <hostname> <port> <nickname>" << std::endl;
        exit(0);
    }

    int sockfd;
    uint16_t portno;

    auto *info = new self_info;
    info->nickname = argv[3];
    info->nickname_size = (uint32_t) sizeof(argv[3]);

    struct sockaddr_in serv_addr{};
    memset(&serv_addr, 0, sizeof(serv_addr));
    struct hostent *server;

    portno = (uint16_t) atoi(argv[2]);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        std::cerr << ("error opening socket") << std::endl;
        exit(1);
    }
    info->socket_is_active = true;
    server = gethostbyname(argv[1]);

    if (server == nullptr)
    {
        std::cerr << ("error: no such host") << std::endl;
        exit(0);
    }

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy(server->h_addr, (char *) &serv_addr.sin_addr.s_addr, (size_t) server->h_length);
    serv_addr.sin_port = htons(portno);

    /* Now connect to the server */
    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        std::cerr << ("error connecting") << std::endl;
        exit(1);
    }

    info->sock = &sockfd;
    if (pthread_mutex_init(&m, nullptr) != 0)
    {
        std::cerr<<"mutex failed"<<std::endl;
        return 1;
    }
    if (pthread_mutex_init(&flag_m, nullptr) != 0)
    {
        std::cerr<<"mutex failed"<<std::endl;
        return 1;
    }

    pthread_t recv_thread, send_thread;
    if (pthread_create(&recv_thread, nullptr, &receive_message, (void*)info)!= 0) {
        std::cerr << "failed to create thread";
        return 1;
    }
    if (pthread_create(&send_thread, nullptr, &send_message, (void*)info) != 0) {
        std::cerr << "failed to create thread";
        return 1;
    }

    pthread_join(recv_thread, nullptr);
    pthread_join(send_thread, nullptr);
    delete info;
}


ssize_t receive_data(const int * sock, server_message* recv_msg)
{
    ssize_t readed = recv(*sock, &(recv_msg->nickname_size), sizeof(recv_msg->nickname_size), 0);
    if (readed == 0)
        return readed;

    recv_msg->nickname_size = ntohl(recv_msg->nickname_size);
    recv_msg->nickname = (char*)malloc(recv_msg->nickname_size);
    readed = recv(*sock, recv_msg->nickname, recv_msg->nickname_size, 0);

    if (readed == 0)
        return readed;

    readed = recv(*sock, &(recv_msg->body_size), sizeof(recv_msg->body_size), 0);
    if (readed == 0)
        return readed;

    recv_msg->body_size = ntohl(recv_msg->body_size);
    recv_msg->body = (char*)malloc(recv_msg->body_size);
    readed = recv(*sock, recv_msg->body, recv_msg->body_size, 0 );
    if (readed == 0)
        return readed;

    readed = recv(*sock, &recv_msg->date_size, sizeof(recv_msg->date_size), 0 );
    if (readed == 0)
        return readed;
    recv_msg->date_size = ntohl(recv_msg->date_size);

    recv_msg->date = (char*)malloc(recv_msg->date_size);
    readed = recv(*sock, recv_msg->date, recv_msg->date_size, 0 );
    if (readed == 0)
        return readed;

    return readed;
}

void * receive_message(void *arg)
{
    auto * info = (self_info *)arg;
    ssize_t recv_status;

    while(true)
    {
        server_message received_message{};
        recv_status = receive_data(info->sock, &received_message);
        if (recv_status == 0)
        {
            std::cout << "Server closed connection, enter any letter to exit" << std::endl;
            pthread_mutex_lock(&flag_m);
            info->socket_is_active = false;
            pthread_mutex_unlock(&flag_m);
            return nullptr;
        }
        pthread_mutex_lock(&m);
        std::cout << "{" << received_message.date << "}  [" << received_message.nickname << "] :  " << received_message.body << std::endl;
        pthread_mutex_unlock(&m);
    }
}


void * send_message(void *arg)
{
    auto * inf = (self_info *)arg;
//    ssize_t MAX_LEN = 1024;
    while(inf->socket_is_active)
    {
        char buffer[1024] ;
        if (!inf->socket_is_active)
            return nullptr;
        char key;
        std::cin >> key;
        std::cin.ignore();

        if (key == 'm') {
            if (!inf->socket_is_active)
                return nullptr;

            pthread_mutex_lock(&m);
            std::cout << "\033[1A\033[K\r" << "You: " << std::flush;
            std::cin.getline(buffer, 1024);
            std::cout << "\033[1A\033[K\r" << std::flush;
            pthread_mutex_unlock(&m);
            uint32_t body_size = htonl(sizeof(buffer));
            ssize_t res;
            uint32_t nn_size = htonl(inf->nickname_size);
            res = send(*inf->sock, &nn_size, sizeof(uint32_t), 0);
            if (res < 1)
            {
                std::cerr<<"Cannot send message. 1" << std::endl;
                return nullptr;
            }

            res = send(*inf->sock, inf->nickname, inf->nickname_size, 0);
            if (res != inf->nickname_size)
            {
                std::cerr<<"Cannot send message. 2" << std::endl;
                return nullptr;
            }

            res = send(*inf->sock, &body_size, sizeof(body_size), 0);
            if (res != sizeof(uint32_t))
            {
                std::cerr<<"Cannot send message. 3" << std::endl;
                return nullptr;
            }

            res = send(*inf->sock, buffer, ntohl(body_size), 0);
            if (res != ntohl(body_size))
            {
                std::cerr<<"Cannot send message. 4" << std::endl;
                return nullptr;
            }
        }
    }
    return nullptr;
}

